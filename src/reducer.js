export default function (state, action) {
    switch (action.type) {
        case 'add':
            return [
                ...state,
                {
                    id: Date.now(),
                    title: action.payload,
                    completed: false,
                    updating: false
                }
            ]
        case 'firstAdd':
            return [
                ...state,
                {
                    id: action.id,
                    title: action.payload,
                    completed: action.completed,
                    updating: false
                }
            ]

        case 'toggle':
            return state.map(todo => {
                if (todo.id === action.payload) {
                    todo.completed = !todo.completed
                }
                return todo
            })

        case "editClick" :
            return state.map(todo => {
                if (todo.id === action.payload) {
                    todo.updating = !action.updating
                }
                return todo
            })

        case "editData" :
            return state.map(todo => {
                if (todo.id === action.payload) {
                    todo.title = action.data
                }
                return todo
            })

        case 'remove':
            return state.filter(todo => todo.id !== action.payload)

        default:
            return state
    }
}