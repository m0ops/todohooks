import React from 'react';
import TodoItem from './TodoItem';
import './TodoList.css'

export default function TodoList({todos}) {
    return (
        <div className="todoList">
            { todos.map(item =>  <TodoItem  key={item.id} {...item} />  )}
        </div>
    )
}