import React, {useEffect, useReducer, useState} from 'react';
import TodoList from './TodoList';
import {Context} from "./context";
import axios from 'axios';
import reducer from "./reducer";
import './App.css';

function App() {

    const [state, dispatch] = useReducer(reducer, JSON.parse(localStorage.getItem('todos')))

    useEffect(() => {
        axios.get(`http://178.128.196.163:3000/api/records/`)
            .then(res => {
                const allData = res.data;

                allData.map( todo =>
                    dispatch({
                        type: 'firstAdd',
                        id: todo._id,
                        payload: todo.data.title,
                        completed: JSON.parse(todo.data.completed)
                    }))
            })
    }, [])


    const [todoTitle, setTodoTitle] = useState('')

    const addTodo = event => {
        if (event.key === "Enter") {
            dispatch({
                type: 'add',
                payload: todoTitle,
                completed: false
            });
            setTodoTitle('')

            let body = {
                "data": {
                    "title": todoTitle,
                    "completed": "false"
                }
            };
            axios.put(`http://178.128.196.163:3000/api/records/`, body)
                .then(res => {})
        }
    }

    return (
        <Context.Provider value={{
            dispatch
        }}>
            <div className='container'>
                <h1>Todo List</h1>
                <div>
                    <label className="labelStyle"><i>Task name</i></label>
                    <input
                        className="inputStyle"
                        value={ todoTitle }
                        onChange={event => {
                            setTodoTitle(event.target.value)
                        }}
                        onKeyPress={ addTodo }
                    />
                    <TodoList todos={ state }/>
                </div>
            </div>
        </Context.Provider>
    )
}

export default App;
