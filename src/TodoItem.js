import React, {useContext} from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';
import {Context} from "./context"
import './TodoItem.css'

export default function TodoItem({id, title, completed, updating}) {

    const {dispatch} = useContext(Context)

    const cls = ["spanTitle"]
    if (completed) {cls.push("completed")}

    let updateButton, updateField;

    if (updating === false) {
        updateButton =  <button
                            className="editButton"
                            onClick={() => dispatch({
                                type: 'editClick',
                                payload: id
                            })}
                        ><i className="material-icons">edit</i>
                        </button>

        updateField = <span className={cls.join(' ')}>{ title }</span>
    } else {
        updateButton =  <button
                            className="editButton"
                            onClick={ saveTitle }
                        ><i className="material-icons">done</i>
                        </button>

        updateField =   <input className={cls.join(' ')}
                             defaultValue={ title }
                             name="editData"
                             onKeyPress={event => {

                                }
                             }
                             onChange={ (e) => dispatch({
                                 type: 'editData',
                                 payload: id,
                                 data: e.target.value
                             })}
                        />
    }

    function saveTitle() {
        let body = {
            "data": {
                "id": id,
                "title": title,
                "completed": completed
            } };

        axios.post(`http://178.128.196.163:3000/api/records/${id}`, body)
            .then(res => {})

        dispatch({
            type: 'editClick',
            payload: id,
            updating: updating
        })
    }
    function saveCheck() {
        dispatch({
            type: 'toggle',
            payload: id })

        let body = {
            "data": {
                "id": id,
                "title": title,
                "completed": !completed
            }};

        axios.post(`http://178.128.196.163:3000/api/records/${id}`, body)
            .then(res => {})
    }
    function deleteTodo() {
        dispatch({
            type: 'remove',
            payload: id
        })
        axios.delete(`http://178.128.196.163:3000/api/records/${id}`)
            .then(res => {})
    }

    return (
        <div key={id} className="todoItem">
            <label className="todoLabel">
                <div className="checkBoxStyle">
                    <Checkbox
                        // defaultChecked={false}
                        checked={ completed }
                        onChange={ saveCheck }
                    /></div>
                {updateField}
                {updateButton}
                <button
                    className="deleteButton"
                    onClick={ deleteTodo }
                ><i className="material-icons">delete</i></button>
            </label>
        </div>
    )
}